@whLogin
Feature: Scenario to verify the Tru Narrative Page

#      Step 1: Open google.com and search for “TruNarrative”
#            Task: Verify that TruNarrative is the first search result returned and that it points to: https://trunarrative.com/
#      Step 2: Click the link found in Step 1 and navigate to the TruNarrative website
#            Task: Verify that the strap line “Easy Onboarding.  Smooth Transactions.  Insightful Compliance.” Is found on the page.
#      Step 3: Navigate via the UI to the TruNarrative Team page
#           Task: Verify that the TruNarrative leadership team consists of 8 people and that the following people have the following roles:


  @whLogin_01
  Scenario Outline: I want to Verify that TruNarrative is the first search result returned and that it points to : https://trunarrative.com/
    Given I open the Google.com page
    When I search for TruNarrative
    Then TruNarrative should be the first search returned and it should point to <URL>
 
    Examples:
      | URL                       | 
      | https://trunarrative.com/ |

  @whLogin_01  
  Scenario Outline: I want to verify the strap line on TruNarrative Webpage
    When I open and navigate to the TruNarrative Website
    Then the <strap line> is found on the page
 
    Examples:
      | strap line                       | 
      | Easy Onboarding.  Smooth Transactions.  Insightful Compliance. |
    
  @whLogin_01  
  Scenario Outline: I want to Verify that the TruNarrative leadership team and their roles:
    Given I open the TruNarrative page
    When I navigate to the TruNarrative Team page
    And team has 8 members
    Then the following <people> have the respective <roles>
 
    Examples:
      | people         |  roles                    |
      | John Lord      |  Founder & CEO            | 
      | David Eastaugh |  Chief Technology Officer |
      | Nicola Janney  |  Human Resources Manager  |

    