package com.wh.pages;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.util.List;

public class GooglePage extends PageObject {
    private static Logger LOGGER = LogManager.getLogger(GooglePage.class);
    
    private static final String SEARCH_ITEM_CSS_SELECTOR = "cite[class='iUh30']";
    
    @FindBy(css = "[class=\"gLFyf gsfi\"]")
    private WebElementFacade googleSearchBox;
    
    @FindBy(css = "input[class='gNO89b'][CSS='2']")
    private WebElementFacade googleSearchButton;
    
    @FindBy(css = "span[class='dTe0Ie qzEoUe'][css='1']")
    private WebElementFacade firstSearch;
    
    @FindBy(css = "div[id='rcnt']")
    private WebElementFacade searchResult;

    
    public void searchForTN()
    {
    	googleSearchBox.typeAndEnter("TruNarrative");
        
    }
    
     public boolean validateFirstSearch(String URL)
    {
    	List<WebElementFacade>select = searchResult.thenFindAll(By.cssSelector(SEARCH_ITEM_CSS_SELECTOR));
    	
    	return select.get(0).getText().equalsIgnoreCase(URL);
    	    
         
    }
   


}
