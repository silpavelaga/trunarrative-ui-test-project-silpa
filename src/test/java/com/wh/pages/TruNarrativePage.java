package com.wh.pages;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.util.List;


public class TruNarrativePage extends PageObject {

    private static Logger LOGGER = LogManager.getLogger(TruNarrativePage.class);
    private static final String SEARCH_MEMBER_CSS_SELECTOR = "h2";
    private static final String NAME_SELECTOR = "//h2[contains(text(),'";
    
    @FindBy(css = "h4[class='bigger']")
    private WebElementFacade strapLine;
    
    @FindBy(css = "li[id='menu-item-6055'] a[href='#']")
    private WebElementFacade more;
    
    @FindBy(css = "a[href='https://trunarrative.com/about-us/meet-the-team/']")
    private WebElementFacade truNarrativeTeam;

    @FindBy(css = "section[class='page pad-bottom team-members thin-content wysiwyg']")
    private WebElementFacade members;
    private int count;
    
    
    public boolean isStrapLineDisplayed(String strapL) {
        return strapLine.getText().equalsIgnoreCase(strapL);
    }
    
    
    public void navigateTeamPage() 
    {
         withAction().moveToElement(element(more)).moveToElement(element(truNarrativeTeam)).click().build().perform();
    }
    
    public int ValidateMemberCount() 
    {
    	List<WebElementFacade> search=members.thenFindAll(By.cssSelector(SEARCH_MEMBER_CSS_SELECTOR));
        count=search.size();
        return count;
    }
    
    
    public boolean ValidateNameResponsibility(String name,String role) {
    	
    	return members.find(By.xpath(NAME_SELECTOR+name+"')]/parent::div/p")).getText().equalsIgnoreCase(role);
		
    	
    }
    
    
}
