package com.wh.exceptions;

public class WhException extends RuntimeException {
    public WhException(final String exceptionMessage) {
        super(exceptionMessage);
    }

    public WhException(final String exceptionMessage, Exception exception) {
        super(exceptionMessage, exception);
    }
}
