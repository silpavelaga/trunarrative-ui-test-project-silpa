package com.wh.stepDefinitions;

import com.wh.serenitySteps.WhSteps;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;

public class WhDefinitions {

    @Steps
    WhSteps whSteps;

    @Given("^I open the Google.com page$")
    public void openGooglePage() {
    	whSteps.openGooglePage();
    }

    @When("^I search for TruNarrative")
    public void searchForTN() {
    	whSteps.searchForTN();
    }
    
    @Then("^TruNarrative should be the first search returned and it should point to (.*)")
    public void validateFirstSearch(String URL) {
    	whSteps.validateFirstSearch(URL);
    }

    @When("^I open and navigate to the TruNarrative Website$")
    public void openTruNarrativePage() {
    	whSteps.openTNPage();
    }
    
    @Then("^the (.*) is found on the page")
    public void validateStrapLine(String strapLine) {
    	whSteps.validateStrapLine(strapLine);
    }


    @Given("^I open the TruNarrative page")
    public void openPage() {
    	whSteps.openTNPage();
    }

    @When("^I navigate to the TruNarrative Team page")
    public void navigateTeamPage() {
    	whSteps.navigateTeamPage();
    }
    
    @When("^team has 8 members")
    public void ValidateMemberCount() {
    	whSteps.ValidateMemberCount();
    }
    
    @Then("^the following (.*) have the respective (.*)$")
    public void ValidateMemberResponsibilityAndCount(String name,String role) {
    		whSteps.ValidateNameResponsibility(name,role);
    }	
   
    	
    }
