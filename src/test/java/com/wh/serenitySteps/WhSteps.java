package com.wh.serenitySteps;

import com.wh.pages.GooglePage;
import com.wh.pages.TruNarrativePage;

import net.thucydides.core.annotations.Step;
import net.thucydides.core.steps.ScenarioSteps;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

public class WhSteps extends ScenarioSteps {
    private static final String Google_PAGE = "https://www.google.com/";
	private static final String TN_PAGE = "https://trunarrative.com/";
    
    private static  String TruNarrative_PAGE;
    
    private  GooglePage onGooglePage() {
        return  getPages().get(GooglePage.class);
    }
    
    private TruNarrativePage onTruNarrativePage() {
        return  getPages().get(TruNarrativePage.class);
    }

    @Step
    public void openGooglePage() {
        onGooglePage().openUrl(Google_PAGE);
    }
    
    public void openTNPage() {
    	onTruNarrativePage().openUrl(TN_PAGE);
    }
    
    public  void searchForTN() {
    	onGooglePage().searchForTN();
    }
    
    public  void validateFirstSearch(String URL) {
    	assertThat("NOT RETURNED AS FIRST SEARCH RESULT",onGooglePage().validateFirstSearch(URL),is(true));
    }
    
    
    public  void validateStrapLine(String strapLine) {
    	assertThat("THE STRAP LINE IS NOT DISPLAYED", onTruNarrativePage().isStrapLineDisplayed(strapLine), is(true));
    }
    
    public  void navigateTeamPage() {
        onTruNarrativePage().navigateTeamPage();
    }
    
    public  void ValidateMemberCount() {
    	assertThat("TEAM PAGE SHOULD HAVE 8 MEMBERS", onTruNarrativePage().ValidateMemberCount(), is(8));
    }
    
    public  void ValidateNameResponsibility(String name,String role) {
    	assertThat("ROLE IS NOT MATCHING", onTruNarrativePage().ValidateNameResponsibility(name,role), is(true));
        
    }

    
    
    

}
