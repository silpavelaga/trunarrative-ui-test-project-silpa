package com.wh.testRunners;

import cucumber.api.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.runner.RunWith;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(features = {"src/test/resources/features/"},
        glue = {"com.wh.stepDefinitions"},
        tags = {"~@wip"})
public class TestRunner {
}
